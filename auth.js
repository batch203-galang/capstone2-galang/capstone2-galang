const jwt = require("jsonwebtoken");
const User = require("./models/User");

const secret = "E-CommerceAPI";


module.exports.createAccessToken = (user) =>{
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	return jwt.sign(data, secret, {});
}


// Token verification
module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization;

	if(token !== undefined){
		token = token.slice(7, token.length);

		//console.log(token);

		return jwt.verify(token, secret, (err, data) =>{
			if(err) {
				return res.send({auth: "Invalid Token!"})
			}
			else {
				next();
			}

			})
	}
	else {
		res.send({message: "Auth failed. No token provided!"})
	}
}

// Token decoding
module.exports.decode = (token) => {
	if(token !== undefined) {
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) =>{
			if(err) {
				// If token is not valid
				return null;
			}
			else {
				return jwt.decode(token, {complete:true}).payload;
			}
		})
	}
	else {
		// If token does not exist
		return null;
	}
}