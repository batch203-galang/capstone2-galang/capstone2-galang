const Product = require("../models/Product");
const auth = require("../auth");


// ADDING PRODUCTS (ADMIN ONLY)
module.exports.addProduct = (req, res) => {

    const userData = auth.decode(req.headers.authorization);

    if (userData.isAdmin == true){

    	let newProduct = new Product ({
        productName: req.body.productName,
        description: req.body.description,
        price: req.body.price,
        stocks: req.body.stocks

        })

        newProduct.save()

        .then(product => {
            console.log(product);
            res.status(200).send(newProduct);
        })

        // Product not saved
        .catch(error => {
            console.log(error);
            res.status(400).send(error);
        })
    } 
    else {
        res.send("You're not an admin!")
    }
};


// RETRIEVING ACTIVE PRODUCTS
module.exports.getAllActive = (req, res) => {
	return Product.find({isActive: true},
		{
			productName: 1,
			description: 1,
			price: 1,
			stocks: 1,
			_id: 0
		})
	.then(result => {
		res.send(result);
	})
	.catch(error => {
		console.log(error);
		res.status(500).send("Can not retrieve products.");
	})
}

// RETRIEVE ALL PRODUCTS (ADMIN ONLY)
module.exports.getAllProducts = (req,res) => {
	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin) {
		return Product.find({})
		.then(result => {
			res.status(200).send(result)
		})
		.catch(error => {
			console.log(error)
			res.status(500).send("Invalid Token")
		})
	}
	else {
        res.status(401).send("You're not an admin!")
    }

}

// RETRIEVE A SPECIFIC PRODUCT
module.exports.getProduct = (req, res) => {
	
	return Product.findById(req.params.productId,
		{
			productName: 1,
			description: 1,
			price: 1,
			stocks: 1,
			_id: 0
		})
		.then(result => {
			res.status(200).send(result)
		})
		.catch(error => {
			console.log(error)
			res.status(500).send("Error on retrieving product details.")
		});
	}


// UPDATE PRODUCT INFORMATION (ADMIN)
module.exports.updateProduct = (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin) {

		let updateProduct = {
			productName: req.body.productName,
			description: req.body.description,
			price: req.body.price,
			stocks: req.body.stocks
			}

		return Product.findByIdAndUpdate(req.params.productId, updateProduct, {new: true})
		.then(result => {
			res.send(result);
		})
		.catch(error => {
			console.log(error);
			res.send(error);
		})
	}
	else {
        res.status(401).send("You're not an admin!")
    }
}

// ARCHIVE PRODUCT (ADMIN)
module.exports.archiveProduct = (req,res) => {

	const userData = auth.decode(req.headers.authorization)

	let updateIsActiveField = {
		isActive: req.body.isActive
	}

	if(userData.isAdmin) {
		return Product.findByIdAndUpdate(req.params.productId, updateIsActiveField, {new:true})
		.then(result => {
			res.send(result);
		})
		.catch(error => {
			console.log(error);
			res.send(error);
		})
	}
	else {
		// return res.send(false);
        res.status(401).send("You're not an admin!");
	}
}


