const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Product = require("../models/Product");


// GET ALL USERS
module.exports.getAllUser = (req,res) => {
	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){
		return User.find({})
		.then(result => res.status(200).send(result))
        .catch(error => res.status(401).send(error))
	}
	else {
		return res.send("You don't have access in this page.")
	}	
}


// REGISTRATION OF USER
module.exports.registerUser = (req, res) => {

	let newUser = new User({
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		password: bcrypt.hashSync(req.body.password, 10),
		mobileNumber: req.body.mobileNumber
	})

	return newUser.save()
	.then(user => {
		res.status(201).send("You have been successfully registered!");
	})
	.catch(error =>{
		console.log(error);
		res.status(500).send("Registration fail. Already have a duplicate");
	})
}


// LOG-IN USER
module.exports.loginUser = (req, res) =>{
	return User.findOne({email: req.body.email})
	.then(result => {
		// User does not exist
		if(result == null) {
			return res.send({message: "No user found!"});
		} 
		// User exists
		else { 
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);

			// If the password match/result of the above code is true.
			if(isPasswordCorrect) {
				return res.send({accessToken: auth.createAccessToken(result)});
			}
			else {
				return res.send({message: "Incorrect password!"});
			}
		}
	})
}

// VIEW PROFILE
module.exports.viewProfile = (req, res) => {
	
	const userData = auth.decode(req.headers.authorization);

	console.log(userData);

	if(userData.isAdmin) {
	return User.findById(userData.id)
	.then(result => {
		result.password = "*****";
		res.send(result);
	})
	.catch(error => {
		console.log(error);
		res.status(500).send(error);
	})

	}

	else {
	return User.findById(userData.id)

	.then(result => {
		const modifiedDetails = {
			firstName: result.firstName,
			lastName: result.lastName,
			email: result.email,
			mobileNumber: result.mobileNumber,
			orders: result.orders
		}
		console.log(result);
		res.send(modifiedDetails);
	})
	.catch(error => {
		console.log(error);
		res.status(500).send(false);
		})
	}		
};

// CREATING AN ORDER
// CREATING AN ORDER
module.exports.order = async (req, res) => {

	const userData = auth.decode(req.headers.authorization)


	if(userData.isAdmin){
		return res.send("You don't have the authorization to order!");
	}
	else{
		let isUserUpdated = await User.findById(userData.id).then(user => {
		user.orders.push({
		totalAmount: req.body.totalAmount,
		products: req.body.products
		})

		// Save the updated user information in the database
		return user.save()
		.then(result => {
			res.send(`Thank you for ordering!`);
		})
		.catch(error => {
			console.log(error);
			res.send(error);
			})
		})
		// console.log(isUserUpdated)
		for (let i=0; i < req.body.products.length; i++){

		let data = {
			userId: userData.id,
			email: userData.email,
			productId: req.body.products[i].productId,
			productName: req.body.products[i].productName,
			quantity: req.body.products[i].quantity
		}

		let isProductUpdated = await Product.findById(data.productId).then(product => {
		product.orders.push({
			userId: data.userId,
			email: data.email,
			quantity: req.body.quantity
		})

		// Minus the stocks available by quantity from req.body
		product.stocks -= data.quantity;

		return product.save()
		.then(result => {
			console.log(result);
			return true;
		})
		.catch(error => {
			console.log(error);
			return false;
				})
			// Condition will check if the both "user" and "product" document have been updated.
			//(isUserUpdated && isProductUpdated) ? res.send(true) : res.send(false)

			})
		}

	}
}


//  ** STRETCH GOALS ** //

// SET USER AS ADMIN (ADMIN ONLY)
module.exports.setUserAsAdmin = (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin) {

		let changeUserToAdmin = {
			isAdmin: req.body.isAdmin
		}

		return User.findByIdAndUpdate(req.params.userId, changeUserToAdmin, {new: true})
		.then(result => {
			res.send(result);
		})
		.catch(error => {
			console.log(error);
			res.send(error);
		})
	}
	else {
        res.status(401).send("You're not an admin!")
    }
}
