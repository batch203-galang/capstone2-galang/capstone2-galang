const express = require("express");
const mongoose = require("mongoose");
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");

const cors = require("cors");

const app = express();

mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.qe9fcvk.mongodb.net/Capstone2_E-commerceAPI?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));

db.once("open", () => console.log("We're connected to the cloud database. Congratulations!"));

// middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// USERS
app.use("/users", userRoutes);

// PRODUCTS
app.use("/products", productRoutes);


const port = process.env.PORT || 4000

app.listen(port, () => {
	console.log(`API is now online on port ${port}`);
});