const express = require("express");
const router = express.Router();
const auth = require("../auth");

const productControllers = require("../controllers/productControllers");

console.log(productControllers);

// > FOR ADDING PRODUCTS (ADMIN ONLY) <
router.post("/addProduct", auth.verify, productControllers.addProduct);

// > FOR VIEWING ALL ACTIVE PRODUCTS <
router.get("/", productControllers.getAllActive);

// > FOR VIEWING ALL PRODUCTS <
router.get("/all", auth.verify, productControllers.getAllProducts); 

// > FOR VIEWING A SPECIFIC PRODUCT <
router.get("/:productId", productControllers.getProduct);

// > FOR UPDATING A COURSE <
router.put("/:productId", auth.verify, productControllers.updateProduct);

// > FOR ARCHIVING A PRODUCT
router.patch("/archive/:productId", auth.verify, productControllers.archiveProduct);

module.exports = router;