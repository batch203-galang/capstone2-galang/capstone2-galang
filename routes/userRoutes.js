const express = require("express");
const router = express.Router();
const auth = require("../auth");
const userControllers = require("../controllers/userControllers");


// > FOR GETTING ALL USER <
router.get("/getAll", auth.verify, userControllers.getAllUser);

// > FOR USER REGISTRATION <
router.post("/register", userControllers.registerUser);

// > FOR USER LOG-IN <
router.post("/login", userControllers.loginUser);

// > FOR VIEWING A USER PROFILE <
router.get("/profile", auth.verify, userControllers.viewProfile);

// > FOR CREATING ORDER <
router.post("/createOrder", auth.verify, userControllers.order);

// ** STRETCH GOALS ** //

// > FOR CHANGING USER TO ADMIN <
router.patch("/:userId/setAsAdmin", auth.verify, userControllers.setUserAsAdmin);


module.exports = router;